﻿using dtSearch.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using dtSearch.Web.Search;

namespace dtSearch.Web
{
  public class Global : System.Web.HttpApplication
  {

    public static IndexJob _SearchIndexJob;

    protected void Application_Start(object sender, EventArgs e)
    {
      SearchIndexer._SearchIndex = Server.MapPath("~/SearchIndex");
    }

    protected void Session_Start(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {

    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {

    }
  }
}