﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using dtSearch.Engine;
using dtSearch.Web;
using dtSearch.Web.Models;
using dtSearch.Web.Search;

public partial class Default : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!Page.IsPostBack) SearchResults = null;
  }

  /// <summary>
  /// Does the search.
  /// </summary>
public IEnumerable<ProductSearchResult> DoSearch(
  string searchTerm,
  int pageNum = 0,
  int pageSize = 10,
  string sortFieldName = "")
{

  var sj = new SearchJob();
  sj.IndexesToSearch.Add(SearchIndexer._SearchIndex);
  sj.MaxFilesToRetrieve = 2000;
  sj.WantResultsAsFilter = true;
  sj.Request = searchTerm;

  // Add filter condition if necessary
  if (!string.IsNullOrEmpty(FacetFilter) && FacetFilter.Contains('='))
  {
    var filter = FacetFilter.Split('=');
    sj.BooleanConditions = string.Format("{0} contains {1}", filter[0], filter[1]);
  }

  // Prevent the code from running endlessly
  sj.AutoStopLimit = 1000;
  sj.TimeoutSeconds = 10;
  sj.Execute();

  this.TotalHitCount = sj.FileCount;
  ExtractFacets(sj);

  // Handle sort requests from the grid
  if (sortFieldName == string.Empty)
  {
    sj.Results.Sort(SortFlags.dtsSortByRelevanceScore, "Name");
  }
  else
  {
    var sortDirection = sortFieldName.ToUpperInvariant().Contains("DESC") ? SortFlags.dtsSortDescending : SortFlags.dtsSortAscending;
    sortFieldName = sortFieldName.Split(' ')[0];
    sj.Results.Sort(SortFlags.dtsSortByField | sortDirection, sortFieldName);
  }

  this.SearchResults = sj.Results;

  // Manual Paging
  var firstItem = pageSize * pageNum;
  var lastItem = firstItem + pageSize;
  lastItem = (lastItem > this.SearchResults.Count) ? this.SearchResults.Count : lastItem;
  var outList = new List<ProductSearchResult>();
  for (int i = firstItem; i < lastItem; i++)
  {
    this.SearchResults.GetNthDoc(i);
    outList.Add(new ProductSearchResult
    {
      DocPosition = i,
      ProductNum = this.SearchResults.DocName,
      Name = this.SearchResults.get_DocDetailItem("Name"),
      Manufacturer = this.SearchResults.get_DocDetailItem("Manufacturer"),
      Age = this.SearchResults.get_DocDetailItem("Age"),
      NumPlayers = this.SearchResults.get_DocDetailItem("NumPlayers"),
      Price = decimal.Parse(this.SearchResults.get_DocDetailItem("Price")) //,
      //HighlightedResults = new HtmlString(FacetedSearch.HighlightResult(this.SearchResults, i))
    });
  }

  return outList;


}

private void ExtractFacets(SearchJob sj)
{

  var filter = sj.ResultsAsFilter;

  var facetsToSearch = new[] { "Manufacturer", "Age", "NumPlayers" };

  // Configure the WordListBuilder to identify our facets
  var wlb = new WordListBuilder();
  wlb.OpenIndex(Server.MapPath("~/SearchIndex"));
  wlb.SetFilter(filter);

  // For each facet or field
  for (var facetCounter = 0; facetCounter < facetsToSearch.Length; facetCounter++)
  {

    // Identify the header for the facet
    var fieldValueCount = wlb.ListFieldValues(facetsToSearch[facetCounter], "", int.MaxValue);
    var thisPanelItem = facets.Items.FindItemByValue(facetsToSearch[facetCounter]);
    thisPanelItem.Items.Clear();

    // For each matching value in the field
    for (var fieldValueCounter = 0; fieldValueCounter < fieldValueCount; fieldValueCounter++)
    {

      string thisWord = wlb.GetNthWord(fieldValueCounter);

      if (string.IsNullOrEmpty(thisWord) || thisWord == "-") continue;

      var label = string.Format("{0}: ({1})", thisWord, wlb.GetNthWordCount(fieldValueCounter));

      var filterValue = string.Format("{0}={1}", facetsToSearch[facetCounter], thisWord);
      thisPanelItem.Items.Add(new RadPanelItem(label) { Value = filterValue });

    }

  }

  }

  protected void searchButton_Click(object sender, EventArgs e)
  {
    SearchResults = null;
    FacetFilter = "";
    grid.DataSource = DoSearch(searchBox.Text.Trim());
    grid.CurrentPageIndex = 0;
    grid.MasterTableView.VirtualItemCount = TotalHitCount;
    grid.DataBind();

  }

  public SearchResults SearchResults
  {
    get { return Session["SearchResults"] as SearchResults; }
    set { Session["SearchResults"] = value; }
  }

  public int TotalHitCount
  {
    get { return (int)Session["TotalHitCount"]; }
    set { Session["TotalHitCount"] = value; }
  }

  public string FacetFilter
  {
    get { return Session["FacetFilter"] == null ? "" : Session["FacetFilter"].ToString(); }
    set { Session["FacetFilter"] = value; }
  }

  protected void grid_PageIndexChanged(object sender, GridPageChangedEventArgs e)
  {
    grid.DataSource = DoSearch(searchBox.Text.Trim(), e.NewPageIndex,
      grid.PageSize, grid.MasterTableView.SortExpressions.GetSortString());
    grid.CurrentPageIndex = e.NewPageIndex;
    grid.DataBind();
  }

  protected void grid_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
  {
    grid.DataSource = DoSearch(searchBox.Text.Trim(), 0, e.NewPageSize, grid.MasterTableView.SortExpressions.GetSortString());
    grid.CurrentPageIndex = 0;
    grid.DataBind();
  }

  protected void grid_SortCommand(object sender, GridSortCommandEventArgs e)
  {

    var sortExpression = string.Concat(e.SortExpression, " ", e.NewSortOrder == GridSortOrder.Ascending ? "ASC" : "DESC");

    grid.DataSource = DoSearch(searchBox.Text.Trim(), 0,
      grid.PageSize, sortExpression);
    grid.CurrentPageIndex = 0;
    grid.DataBind();
  }

protected void facets_ItemClick(object sender, RadPanelBarEventArgs e)
{
  SearchResults = null;
  FacetFilter = e.Item.Value;
  grid.DataSource = DoSearch(searchBox.Text.Trim());
  grid.CurrentPageIndex = 0;
  grid.MasterTableView.VirtualItemCount = TotalHitCount;
  grid.DataBind();
}

protected void tipMgr_AjaxUpdate(object sender, ToolTipUpdateEventArgs e)
{

  var newDiv = new HtmlGenericControl("div");
  newDiv.InnerHtml = FacetedSearch.HighlightResult(this.SearchResults, int.Parse(e.Value));
  e.UpdatePanel.ContentTemplateContainer.Controls.Add(newDiv);
}

protected void grid_ItemDataBound(object sender, GridItemEventArgs e)
{
  var thisRow = e.Item;
  if (thisRow.ItemType == GridItemType.Item || thisRow.ItemType == GridItemType.AlternatingItem)
  {
    var dataItem = thisRow.DataItem as ProductSearchResult;
    tipMgr.TargetControls.Add(thisRow.ClientID, dataItem.DocPosition.ToString(), true);
  }
}
}
