﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace dtSearch.Web.Models
{
  public partial class GameShopEntities
  {

    public GameShopEntities(string connString) : base(connString)
    {

    }

    public static GameShopEntities New()
    {
      var connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
      connString = string.Format("metadata=res://*/Models.GameShop.csdl|res://*/Models.GameShop.ssdl|res://*/Models.GameShop.msl;provider=System.Data.SqlClient;provider connection string=\"{0}\"",
        connString);
      return new GameShopEntities(connString);
    }

  }

}