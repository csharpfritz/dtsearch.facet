﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace dtSearch.Web.Models
{
  public class ProductSearchResult
  {

    public int DocPosition { get; set; }

    public string ProductNum { get; set; }

    public string Name { get; set; }

    public string LongDescription { get; set; }

    public string Age { get; set; }

    public string NumPlayers { get; set; }

    public string Manufacturer { get; set; }

    public decimal Price { get; set; }

    public HtmlString HighlightedResults { get; set; }

  }
}