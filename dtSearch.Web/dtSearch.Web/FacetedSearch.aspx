﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FacetedSearch.aspx.cs" Inherits="dtSearch.Web.FacetedSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <style>
    .searchHit { background-color: yellow; color: red; text-decoration: underline;
  </style>
</head>
<body>
    <form id="form1" runat="server">
<div>
    
  <asp:Label runat="server" ID="lSearch" AssociatedControlID="txtSearch" Text="Search Term:"></asp:Label>
  <asp:TextBox runat="server" ID="txtSearch"></asp:TextBox>
  <asp:Button runat="server" ID="bDoSearch" Text="Search" OnClick="bDoSearch_Click" />

</div>

<asp:Panel runat="server" ID="pFacets" Width="200" style="float: left;">
        
</asp:Panel>

<asp:GridView runat="server" ID="resultsGrid" OnPageIndexChanging="results_PageIndexChanging" 
  AllowPaging="true" AllowCustomPaging="true" AutoGenerateColumns="false" 
  ItemType="dtSearch.Web.Models.ProductSearchResult" ShowHeader="false" BorderWidth="0">
  <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />
  <Columns>
    <asp:TemplateField>
      <ItemTemplate>
        <a href='/Products/<%#: Item.ProductNum  %>' class="productName"><%#: Item.Name %></a><br />
        <%#: Item.HighlightedResults %>

      </ItemTemplate>
    </asp:TemplateField> 
  </Columns>
</asp:GridView>

    </form>
</body>
</html>
