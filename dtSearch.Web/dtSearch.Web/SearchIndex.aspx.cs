﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dtSearch.Web.Search;

namespace dtSearch.Web
{
  public partial class SearchIndex : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      var s = new SearchIndexer();
      s.BuildIndex();
    }
  }
}