﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title></title>
  <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server" />
  <style>
    .searchHit { background-color: yellow; color: red; text-decoration: underline;
  </style>
</head>
<body>
  <form id="form1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
      <Scripts>
        <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
        <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
        <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
      </Scripts>
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager runat="server" ID="ajaxMgr" UpdatePanelsRenderMode="Inline" >
      <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="grid">
          <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="grid" />
            <telerik:AjaxUpdatedControl ControlID="tipMgr" />
          </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="searchButton">
          <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="grid" />
            <telerik:AjaxUpdatedControl ControlID="tipMgr" />
            <telerik:AjaxUpdatedControl ControlID="facets" />
          </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="facets">
          <UpdatedControls>
            <telerik:AjaxUpdatedControl ControlID="grid" />
            <telerik:AjaxUpdatedControl ControlID="tipMgr" />
          </UpdatedControls>
        </telerik:AjaxSetting>
      </AjaxSettings>
    </telerik:RadAjaxManager>
<telerik:RadToolTipManager runat="server" ID="tipMgr" Animation="Fade" OnAjaxUpdate="tipMgr_AjaxUpdate" RelativeTo="Element"
  width="400px" Height="150px" HideDelay="30000" RenderInPageRoot="true">
</telerik:RadToolTipManager> 

    <div>

      <telerik:RadFormDecorator runat="server" ID="formDeco" DecoratedControls="All" />

      <fieldset>
        <asp:TextBox runat="server" ID="searchBox" ClientIDMode="Static"></asp:TextBox>
        <asp:Button runat="server" ID="searchButton" Text="Search" OnClick="searchButton_Click" />
      </fieldset>

<telerik:RadSplitter runat="server" Orientation="Vertical" Width="100%" Height="600">
<telerik:RadPane runat="server" Width="150" MinWidth="150" MaxWidth="300">
          
<telerik:RadPanelBar runat="server" ID="facets" Width="100%" OnItemClick="facets_ItemClick">
  <Items>
    <telerik:RadPanelItem runat="server" Text="Manufacturer" Value="Manufacturer"></telerik:RadPanelItem>
    <telerik:RadPanelItem runat="server" Text="Age" Value="Age"></telerik:RadPanelItem>
    <telerik:RadPanelItem runat="server" Text="# Players" Value="NumPlayers"></telerik:RadPanelItem>
  </Items>
</telerik:RadPanelBar>

</telerik:RadPane>
<telerik:RadSplitBar runat="server"></telerik:RadSplitBar>
<telerik:RadPane runat="server">
<telerik:RadGrid runat="server" ID="grid" PageSize="10" Height="100%" 
  OnPageIndexChanged="grid_PageIndexChanged" 
  OnPageSizeChanged="grid_PageSizeChanged" 
  OnSortCommand="grid_SortCommand"
  OnItemDataBound="grid_ItemDataBound"
  AllowPaging="true" AllowCustomPaging="true" 
  AllowSorting="true" 
  EnableHeaderContextFilterMenu="false">
  <MasterTableView AutoGenerateColumns="false" 
    PagerStyle-AlwaysVisible="true"
    AlternatingItemStyle-BackColor="LightBlue" 
    ItemType="dtSearch.Web.Models.ProductSearchResult">
    <Columns>
      <telerik:GridBoundColumn DataField="ProductNum" HeaderText="Product ID" />
      <telerik:GridBoundColumn DataField="Name" HeaderText="Name" />
      <telerik:GridBoundColumn UniqueName="Manufacturer" DataField="Manufacturer" HeaderText="Manufacturer" />
      <telerik:GridBoundColumn UniqueName="Age" DataField="Age" HeaderText="Recommended Age" />
      <telerik:GridBoundColumn UniqueName="NumPlayers" DataField="NumPlayers" HeaderText="# Players" />
      <telerik:GridBoundColumn DataField="Price" HeaderText="Price" SortExpression
        DataFormatString="{0:$0.00}" ItemStyle-HorizontalAlign="Right" />
    </Columns>
  </MasterTableView>
  <ClientSettings Resizing-AllowColumnResize="true">
             
  </ClientSettings>
</telerik:RadGrid>
  <!-- FACET SEARCH BAR GOES HERE -->
  <!-- SEARCH RESULTS GO HERE -->
        </telerik:RadPane>
      </telerik:RadSplitter>

    </div>
  </form>
</body>
</html>
