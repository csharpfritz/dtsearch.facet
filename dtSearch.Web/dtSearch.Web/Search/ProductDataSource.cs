using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using dtSearch.Web.Models;

namespace dtSearch.Web.Search
{
  public class ProductDataSource : dtSearch.Engine.DataSource
  {

private GameShopEntities _GameShopContext = null;
private int _RecordNumber = 0;
    private Product _CurrentProduct;

    private IEnumerable<Product> _CurrentBatch;

    public static readonly string[] ProductFields = new[] {
      "Name",
      "Description",
      "Weight",
      "LongDesc",
      "Age",
      "NumPlayers",
      "Price",
      "Manufacturer"
    };

    private int _TotalRecords;

    public ProductDataSource()
    {
      DocIsFile = false;
      DocFields = string.Empty;
      DocText = "";

    }

    /// <summary>
    /// Rewinds this instance.
    /// </summary>
    /// <returns></returns>
public override bool Rewind()
{

  // Initialize a single EF context
  if (this._GameShopContext == null)
  {
    // New() is a static method that passes config string
    this._GameShopContext = GameShopEntities.New();
  }

  _RecordNumber = 0;
  this._TotalRecords = _GameShopContext.Products.Count();
  return true;

}

    /// <summary>
    /// Gets the next doc.
    /// </summary>
    /// <returns></returns>
public override bool GetNextDoc()
{

  // Exit now if we are at the end of the record set
  if (_TotalRecords <= _RecordNumber + 1) return false;

  // Reset Properties
  DocName = "";
  DocDisplayName = "";
  DocModifiedDate = DateTime.Now;
  DocCreatedDate = DateTime.Now;

  // Get the product from the data source
  _CurrentProduct = _GameShopContext.Products.OrderBy(p => p.ProductNo).Skip(_RecordNumber).First();

  FormatDocFields(_CurrentProduct);
  DocName = _CurrentProduct.ProductNo;
  DocStream = GetStreamForProduct(_CurrentProduct);

  var lastUpdated = _CurrentProduct.LastUpdated;
  lastUpdated = lastUpdated == null ? "20090101130000" : lastUpdated;
  DocModifiedDate = DateTime.ParseExact(lastUpdated.Substring(0, 14), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
  DocCreatedDate = new DateTime(2000, 1, 1);

  _RecordNumber++;
  return true;

}
  
    /// <summary>
    /// Gets the stream for product.
    /// </summary>
    /// <param name="product">The _ current product.</param>
    /// <returns></returns>
    private Stream GetStreamForProduct(Product product)
    {

      var sb = new StringBuilder();
//      sb.AppendFormat("<span class='name'>{0}</span>", product.Name);
      sb.Append("<html><dl>");
      var ddFormat = "<dt>{0}</dt><dd>{1}</dd>";
      sb.AppendFormat(ddFormat, "Description", product.LongDesc);
      sb.AppendFormat(ddFormat, "Manufacturer", product.Manufacturer);
      sb.Append("</dl></html>");

      var ms = new MemoryStream();
      var sw = new StreamWriter(ms);
      sw.Write(sb.ToString());
      sw.Flush();

      return ms;

    }

    /// <summary>
    /// Formats the doc fields.
    /// </summary>
    /// <param name="_CurrentProduct">The _ current product.</param>
private void FormatDocFields(Product product)
{

  DocFields = "";

  StringBuilder sb = new StringBuilder();

  var type = product.GetType();
  foreach (var item in ProductFields)
  {
    var fieldValue = type.GetProperty(item).GetValue(product);
    if (sb.Length > 0) sb.Append("\t");
    sb.AppendFormat("{0}\t{1}", item, fieldValue == null ? "" : fieldValue.ToString().Replace("\t"," "));
  }

  /*
   *   var format = "{0}\t{1}\t";
  sb.AppendFormat(format, "Name", product.Name ?? "");
  sb.AppendFormat(format, "Description", product.Description ?? "");
  sb.AppendFormat(format, "Weight", product.Weight.HasValue ? product.Weight.Value : 0);
  sb.AppendFormat(format, "LongDesc", product.LongDesc ?? "");
  sb.AppendFormat(format, "Age", product.Age ?? "");
  sb.AppendFormat(format, "NumPlayers", product.NumPlayers ?? "");
  sb.AppendFormat(format, "Price", product.Price.HasValue ? product.Price.Value: 0.00M);
  sb.AppendFormat(format, "Manufacturer", product.Manufacturer ?? "");
*/
  DocFields = sb.ToString();

}

  }

}