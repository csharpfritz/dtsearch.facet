﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using dtSearch.Engine;

namespace dtSearch.Web.Search
{
  public class SearchIndexer
  {

    public static string _SearchIndex = string.Empty;

    public void BuildIndex()
    {

      using (var indexJob = new IndexJob())
      {
        var dataSource = new ProductDataSource();
        indexJob.DataSourceToIndex = dataSource;
        indexJob.IndexPath = _SearchIndex;
        indexJob.ActionCreate = true;
        indexJob.ActionAdd = true;
        indexJob.CreateRelativePaths = false;

        // Create the faceted index
        indexJob.EnumerableFields = new StringCollection() { "Description","LongDesc", "Age", "NumPlayers", "Price", "Manufacturer" };
          //"Age,NumPlayers,Price"};

        var sc = new StringCollection();
        sc.AddRange(ProductDataSource.ProductFields);

        indexJob.StoredFields = sc;
        indexJob.IndexingFlags = IndexingFlags.dtsIndexCacheTextWithoutFields | IndexingFlags.dtsIndexCacheOriginalFile;

        ExecuteIndexJob(indexJob);
      }

    }
  
    /// <summary>
    /// Executes the index job.
    /// </summary>
    /// <param name="indexJob">The index job.</param>
    private void ExecuteIndexJob(IndexJob indexJob)
    {
      indexJob.Execute();
    }


  }

}