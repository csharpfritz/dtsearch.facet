﻿using System.Web.UI.HtmlControls;
using dtSearch.Engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dtSearch.Web.Models;
using dtSearch.Web.Search;

namespace dtSearch.Web
{
  public partial class FacetedSearch : System.Web.UI.Page
  {

    const int PageSize = 10;
    private SearchResults _SearchResults;
    private string filter = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

      if (!Page.IsPostBack && !string.IsNullOrEmpty(Request.QueryString["s"]))
      {
        txtSearch.Text = Request.QueryString["s"];
        DoSearch(0);
      }

    }

    protected void bDoSearch_Click(object sender, EventArgs e)
    {
      DoSearch(0);
    }

public void DoSearch(int pageNum)
{

  // Configure and execute search
  var sj = new SearchJob();
  sj.IndexesToSearch.Add(SearchIndexer._SearchIndex);
  sj.MaxFilesToRetrieve = (pageNum+1) * PageSize;
  sj.WantResultsAsFilter = true;
  sj.Request = txtSearch.Text.Trim();

  // Add filter condition if necessary
  if (!string.IsNullOrEmpty(Request.QueryString["f"]))
  {
    sj.BooleanConditions = string.Format("{0} contains {1}", Request.QueryString["f"], Request.QueryString["t"]);
  }

  sj.AutoStopLimit = 1000;
  sj.TimeoutSeconds = 10;
  sj.Execute();

  ExtractFacets(sj);

  // Present results
  sj.Results.Sort(SortFlags.dtsSortByRelevanceScore, "Name");
  this._SearchResults = sj.Results;

  // Manual Paging
  var firstItem = PageSize * pageNum;
  var lastItem = firstItem + PageSize;
  lastItem = (lastItem > _SearchResults.Count) ? _SearchResults.Count : lastItem;
  var outList = new List<ProductSearchResult>();
  for (int i = firstItem; i < lastItem; i++)
  {
    _SearchResults.GetNthDoc(i);
    outList.Add(new ProductSearchResult
    {
      ProductNum = _SearchResults.DocName,
      Name = _SearchResults.get_DocDetailItem("Name"),
      HighlightedResults = new HtmlString(HighlightResult(_SearchResults, i))
    });
  }

  // Configure and bind to the grid virtually, so we don't load everything
  resultsGrid.DataSource = outList;
  resultsGrid.PageIndex = pageNum;
  resultsGrid.VirtualItemCount = sj.FileCount;
  resultsGrid.DataBind();

}
  
    /// <summary>
    /// Extracts the facets.
    /// </summary>
    /// <param name="sj">The sj.</param>
private void ExtractFacets(SearchJob sj)
{

  var filter = sj.ResultsAsFilter;
  var facetsToSearch = new[] { "Manufacturer", "Age", "NumPlayers" };

  // Configure the WordListBuilder to identify our facets
  var wlb = new WordListBuilder();
  wlb.OpenIndex(SearchIndexer._SearchIndex);
  wlb.SetFilter(filter);

  // For each facet or field
  for (var facetCounter = 0; facetCounter < facetsToSearch.Length; facetCounter++)
  {

    // Construct a header for the facet
    var fieldValueCount = wlb.ListFieldValues(facetsToSearch[facetCounter], "", int.MaxValue);
    var thisPanelItem = new HtmlGenericControl("div");
    var header = new HtmlGenericControl("h4");
    header.InnerText = facetsToSearch[facetCounter];
    thisPanelItem.Controls.Add(header);

    // For each matching value in the field
    for (var fieldValueCounter = 0; fieldValueCounter < fieldValueCount; fieldValueCounter++)
    {
      string thisWord = wlb.GetNthWord(fieldValueCounter);
      int thisWordCount = wlb.GetNthWordCount(fieldValueCounter);

      if (string.IsNullOrEmpty(thisWord) || thisWord == "-") continue;

      thisPanelItem.Controls.Add(new HtmlAnchor() { InnerText = string.Format("{0} ({1})", thisWord, thisWordCount) , HRef = "FacetedSearch.aspx?s=" + txtSearch.Text + "&f=" + facetsToSearch[facetCounter] + "&t=" + thisWord });
      thisPanelItem.Controls.Add(new HtmlGenericControl("br"));

    }

    pFacets.Controls.Add(thisPanelItem);

  }

}

    /// <summary>
    /// Highlights the result.
    /// </summary>
    /// <param name="item">The item.</param>
internal static string HighlightResult(SearchResults results, int itemPos)
{

  using (FileConverter fc = new FileConverter())
  {

    fc.SetInputItem(results, itemPos);
    fc.OutputFormat = OutputFormats.itHTML;
    fc.OutputToString = true;
    fc.OutputStringMaxSize = 200000;
    fc.BeforeHit = "<span class='searchHit'>";
    fc.AfterHit = "</span>";

    // TODO:  This doesn't feel right - double outputting content..
    fc.Flags = ConvertFlags.dtsConvertGetFromCache;
    fc.Execute();

    return fc.OutputString.Substring(0, fc.OutputString.IndexOf("</dl>")+5);

  }

}



    protected void results_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      DoSearch(e.NewPageIndex);
    }


  }
}